Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do
      resources :user, only: [:create, :update, :show]
      resources :project, only: [:create, :update, :show, :index]
      resources :time, only: [:create, :update, :show]
    
      post 'authenticate', to: 'authentication#authenticate'
    end
  end

end
