class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :login, unique: true
      t.string :password

      t.timestamps
    end

    add_index :users, :login, unique: true
  end
end