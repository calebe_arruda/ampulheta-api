class CreateClocks < ActiveRecord::Migration[5.1]
  def change
    create_table :clocks do |t|
      t.datetime :started_at
      t.datetime :ended_at
      t.references :project, foreign_key: true

      t.timestamps
    end
  end
end
