User.create(
  name: "ampulheta-api",
  login: "ampulheta-api",
  email: "ampulheta@amp.com",
  password: "Amp@26"
)

10.times do
  User.create(
    name: Faker::Name.name,
    login: Faker::Internet.user_name,
    email: Faker::Internet.free_email,
    password: Faker::Internet.password)
end

15.times do
  Project.create(
    title: Faker::Hobbit.character,
    description: Faker::Hobbit.quote,
    users: User.find([1,2,3,4,5])
  )
end

15.times do |id|
  5.times do
    Clock.create(
      project_id: id,
      started_at: Faker::Time.backward(10, :morning),
      ended_at: Faker::Time.forward(5, :night)
    )
  end
end