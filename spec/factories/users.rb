FactoryBot.define do
  factory :user do
    name { Faker::StarWars.character }
    email 'test@test.com'
    login { Faker::Internet.user_name }
    password 'test@222'
    password_confirmation 'test@222'
  end
end