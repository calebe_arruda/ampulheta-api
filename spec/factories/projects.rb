FactoryBot.define do
  factory :project do
    title { Faker::Movie.quote }
    description { Faker::StarWars.quote }
  end
end