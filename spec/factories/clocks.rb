FactoryBot.define do
  factory :clock do
    started_at { Faker::Time.backward(4, :morning) }
    ended_at { Faker::Time.forward(5, :morning) }
    project_id { create(:project).id }
  end
end