module RequestSpecHelper
  # Helper to parse a JSON file in the tests
  def json
    JSON.parse(response.body)
  end
end