require 'rails_helper'

RSpec.describe Clock, type: :model do

  it { should validate_presence_of(:started_at) }
  it { should validate_presence_of(:ended_at) }
  it { should belong_to(:project) }

end
