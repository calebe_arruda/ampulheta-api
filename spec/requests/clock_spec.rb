require 'rails_helper'

RSpec.describe 'Clock API', type: :request do
  let!(:user) { create(:user) }
  let!(:times) { create_list(:clock, 10) }
  let(:time_id) { times.first.id }
  let!(:project) { create(:project) }
  let(:project_id) { project.id }
  let(:headers) { valid_headers }

  # Test suite for GET /api/v1/time/:project_id
  describe 'GET /api/v1/time/:project_id' do
    before { get "/api/v1/time/#{project_id}", params: {}, headers: headers }

    context 'when times exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end

  # Test suite for POST /api/v1/time
  describe 'POST /api/v1/time' do
    let(:valid_attributes) do
      { 
        started_at: DateTime.current().at_beginning_of_day(), 
        ended_at: DateTime.current().at_end_of_day(),
        project_id: project_id
       }.to_json
    end

    context 'when request attributes are valid' do
      before { post '/api/v1/time', params: valid_attributes, headers: headers }
      
      it 'creates a time' do
        expect(json['time']['started_at']) == DateTime.current().at_beginning_of_day().to_s(:db)
        expect(json['time']['ended_at']) == DateTime.current().at_end_of_day().to_s(:db)
        expect(json['time']['project_id']).to eq(project_id)
      end
      
      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end
    
    context 'when an invalid request' do
      before { post '/api/v1/time', params: {}, headers: headers }
      
      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end
  end
end