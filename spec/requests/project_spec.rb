require 'rails_helper'

RSpec.describe 'Project API', type: :request do
  let(:user) { create(:user) }
  let!(:users) { create_list(:user, 15) }
  let!(:projects) { create_list(:project, 10) }
  let(:project_id) { projects.first.id }
  let(:headers) { valid_headers }

  # Test suite for GET /api/v1/project
  describe 'GET /api/v1/project' do
    before { get '/api/v1/project', params: {}, headers: headers }

    context 'when projects exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'return all projects' do
        expect(json['projects'].size).to eq(10)
      end
    end

  end
  
  # Test suite for GET /api/v1/project/:id
  describe 'GET /api/v1/project/:id' do
    before { get "/api/v1/project/#{project_id}", params: {}, headers: headers }
    
    context 'when project exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
      
      it 'returns the project' do
        expect(json['project']['id']).to eq(project_id)
      end
    end
    
    context 'when project does not exist' do
      let(:project_id) { 999 }
      before { get "/api/v1/project/#{project_id}", params: {}, headers: headers }
  
      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end
  
      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Project/)
      end
    end
  end

  # Test suite for POST /project
  describe 'POST /api/v1/project' do
    let(:valid_attributes) do
       { title: 'SpaceShip', description: 'Build a spaceship' }.to_json
    end

    context 'when request attributes are valid' do
      before { post '/api/v1/project', params: valid_attributes, headers: headers }

      it 'creates a project' do
        expect(json['project']['title']).to eq('SpaceShip')
        expect(json['project']['description']).to eq('Build a spaceship')
        # expect(json['users'].size).to eq(10)
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when an invalid request' do
      before { post "/api/v1/project", params: {}, headers: headers }

      it 'return status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Title can't be blank/)
      end
    end
    
    # Test suite for PUT /api/v1/project/:id
    describe 'PUT /api/v1/project/:id' do
      let(:valid_request) do
         { title: 'StarTrek', user_id: [users.first.id, users.second.id] }.to_json 
      end

      context 'when request attributes are valid' do
        before { put "/api/v1/project/#{project_id}", params: valid_request, headers: headers }
        
        it 'returns status code 202' do
          expect(response).to have_http_status(202)
        end
        
        it 'returns a valid json updated' do
          expect(json['project']['title']).to match(/StarTrek/)
          # expect(json['users'].size).to eq(2)
        end
      end
      
      context 'when the project does not exist' do
        let(:project_id) { 999 }
        before { put "/api/v1/project/#{project_id}", params: valid_request, headers: headers }

        it 'returns status code 404' do
          expect(response).to have_http_status(404)
        end

        it ('returns a not found message') do
          expect(response.body).to match(/Couldn't find Project/)
        end
      end
    end
  end
end