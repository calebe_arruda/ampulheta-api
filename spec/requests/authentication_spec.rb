require 'rails_helper'

RSpec.describe 'Authentication', type: :request do
  describe 'POST /api/v1/authenticate' do
    let!(:user) { create(:user) }
    let(:headers) { valid_headers.except('Authorization') }
    let(:valid_credentials) do
      {
        login: user.login,
        password: user.password
      }.to_json
    end
    let(:invalid_credentials) do
      {
        login: Faker::Internet.user_name,
        password: Faker::Internet.password
      }.to_json
    end

    context 'when request is valid' do
      before { post '/api/v1/authenticate', params: valid_credentials, headers: headers }

      it 'returns an authentication token' do
        expect(json['token']).not_to be_nil
      end
    end

    context 'when request is invalid' do
      before { post '/api/v1/authenticate', params: invalid_credentials, headers: headers }

      it 'returns a failure message' do
        expect(json['message']).to match(/Invalid credentials/)
      end
    end
  end
end