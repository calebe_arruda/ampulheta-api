require 'rails_helper'

RSpec.describe 'User API', type: :request do
  
  let(:user) { create(:user) }
  let!(:users) { create_list(:user, 10) }
  let(:user_id) { users.first.id }
  let(:headers) { valid_headers }
  let(:valid_attributes) do
    attributes_for(:user, password_confirmation: user.password)
  end

  # Test suite for GET /api/v1/user/:id
  describe 'GET /api/v1/user/:id' do
    before { get "/api/v1/user/#{user_id}", params: {}, headers: headers }

    context 'when the user exists' do
      it 'returns the user' do
        expect(json).not_to be_empty
        expect(json['user']['id']).to eq(user_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the user does not exist' do
      let(:user_id) { 999 }

      it 'return status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find User/)
      end
    end

    # Test suit for POST /api/v1/user
    describe 'POST /api/v1/user' do
      let(:valid_attributes) { { name: 'Francisco Alves', login: 'franc', email: 'francisco.alves@gmail', password: 'fr12al@' }.to_json }

      context 'when the request is valid' do
        before { post '/api/v1/user', params: valid_attributes, headers: headers }

        it 'creates a user' do
          expect(json['user']['name']).to eq('Francisco Alves')
          expect(json['user']['email']).to eq('francisco.alves@gmail')
          expect(json['user']['login']).to eq('franc')
        end

        it 'returns status code 201' do
          expect(response).to have_http_status(201)
        end
      end

      context 'when the request is invalid' do
        it 'returns status code 422' do
          post '/api/v1/user', params: { name: 'Bart Simpsons', email: 'bart@hotmail.com', password: 'fr12al@' }.to_json, headers: headers
          expect(response).to have_http_status(422)
        end
        
        it 'returns a validation login failure message' do
          post '/api/v1/user', params: { name: 'Bart Simpsons', email: 'bart@hotmail.com', password: 'fr12al@' }.to_json, headers: headers
          expect(response.body).to match(/Validation failed: Login can't be blank/)
        end
        
        it 'returns a validation name failure message' do
          post '/api/v1/user', params: { login: 'bart', email: 'bart@hotmail.com', password: 'fr12al@' }.to_json, headers: headers
          expect(response.body).to match(/Validation failed: Name can't be blank/)
        end

        it 'returns a validation email failure message' do
          post '/api/v1/user', params: { login: 'bart', name: 'Bart Simpsons', password: 'fr12al@' }.to_json, headers: headers
          expect(response.body).to match(/Validation failed: Email can't be blank/)
        end

        it 'returns a validation password failure message' do
          post '/api/v1/user', params: { login: 'bart', name: 'Bart Simpsons', email: 'bart@hotmail.com' }.to_json, headers: headers
          expect(response.body).to match(/Validation failed: Password can't be blank/)
        end
      end
    end

    # Test suite for PUT /api/v1/user/:id
    describe 'PUT /api/v1/user/:id' do
      let(:valid_attributes) { { name: 'Homer Simpsons', email: 'h.simpsons@gmail.com', login: 'homer' }.to_json }

      context 'when the user exists' do
        before { put "/api/v1/user/#{user_id}", params: valid_attributes, headers: headers }

        it 'updates the user' do
          expect(json['user']['name']).to eq('Homer Simpsons')
          expect(json['user']['email']).to eq('h.simpsons@gmail.com')
          expect(json['user']['login']).to eq('homer')
        end

        it 'returns status code 202' do
          expect(response).to have_http_status(202)
        end
      end
    end
  end
end
