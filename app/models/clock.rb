class Clock < ApplicationRecord
  belongs_to :project

  validates_presence_of :started_at, :ended_at
end
