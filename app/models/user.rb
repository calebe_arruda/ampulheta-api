class User < ApplicationRecord
    has_secure_password

    has_and_belongs_to_many :projects

    validates_presence_of [:name, :login, :email, :password_digest]
    validates_uniqueness_of :login, case_sensitive: false
end
