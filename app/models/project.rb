class Project < ApplicationRecord
    has_and_belongs_to_many :users
    has_many :clocks

    validates_presence_of :title
end
