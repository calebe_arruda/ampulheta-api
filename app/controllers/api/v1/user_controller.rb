module Api
  module V1
    class UserController < ApplicationController
      skip_before_action :authorize_request, only: :signup
      before_action :set_user, only: [:show, :update]
    
      # POST /user
      def create
        @user = User.create!(user_params)
        json_response(@user, :created)
      end
    
      def signup
        user = User.create!(user_params)
        auth_token = AuthenticateUser.new(user.login, user.password).call
        response = { message: Message.account_created, auth_token: auth_token }
        json_response(response, :created)
      end
    
      # GET /user/:id
      def show
        json_response(@user)
      end
    
      # PUT /user/:id
      def update
        if @user.update!(user_params)
          @user = set_user
          json_response(@user, :accepted)
        end
      end
    
      private
    
      def user_params
        params.permit(:name, :login, :email, :password, :password_confirmation)
      end
    
      def set_user
        @user = User.find(params[:id])
      end
    end
  end
end
