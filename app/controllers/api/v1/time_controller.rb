module Api
  module V1
    class TimeController < ApplicationController
      before_action :set_time, only: [:update]

      # GET /time/:project_id
      def show
        @times = Clock.where(project_id: time_params[:id])
        json_response(@times)        
      end

      # POST /time
      def create
        @time = Clock.create!(time_params)
        json_response(@time, :created)
      end

      # PUT /time
      def update
        if @time.update!(time_params)
          @time = set_time
          json_response(@time, :accepted)
        end
      end

      private 

      def set_time
        @time = Clock.find(time_params[:id])
      end

      def time_params
        params.permit(:started_at, :ended_at, :project_id, :id)
      end
    end
  end
end
