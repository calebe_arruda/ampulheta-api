module Api
  module V1
    class ProjectController < ApplicationController
      before_action :set_project, only: [:show, :update]
      before_action :create_project, only: [:create]
      
      # GET /project
      def index
        @projects = Project.all
        json_response(@projects)
      end
    
      # POST /project
      def create
        json_response(@project, :created)
      end
      
      # GET /project/:id
      def show
        json_response(@project)
      end
      
      # PUT /project/:id
      def update
        update_users
        if @project.update!(project_params)
          @project = set_project
          json_response(@project, :accepted)
        end
      end
      
      private
      
      def create_project
        @project = Project.new(project_params)
        update_users
        @project.save!
      end
      
      def update_users
        if params['user_id']
          @users = User.find(params['user_id'])
          @project.users = @users
        end
      end
    
      def project_params
        params.permit(:title, :description)
      end
    
      def set_project
        @project = Project.find(params[:id])
      end
    end
  end
end
