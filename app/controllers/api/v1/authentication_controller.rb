module Api
  module V1
    class AuthenticationController < ApplicationController
      skip_before_action :authorize_request, only: :authenticate
    
      def authenticate
        auth_token = AuthenticateUser.new(auth_params[:login], auth_params[:password]).call
        json_response(create_json_response_auth(auth_token))
      end
    
      private

      def create_json_response_auth(auth_token)
        @user = UserSerializer.new(User.find_by(login: auth_params[:login]))
        { token: auth_token, user: @user }
      end
    
      def auth_params
        params.permit(:login, :password)
      end

    end
  end
end