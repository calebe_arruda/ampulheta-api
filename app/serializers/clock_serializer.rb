class ClockSerializer < ActiveModel::Serializer
  type :time

  attributes :id, :started_at, :ended_at, :project_id

  belongs_to :project
end
