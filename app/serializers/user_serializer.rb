class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :login, :email

  has_many :projects
end
