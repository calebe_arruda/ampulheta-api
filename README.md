# Desafio - Ampulheta

## Descrição

Está API foi desenvolvida para atender profissionais que tinham a necessidade de apontar 
horas de trabalho em projetos associados que estavam trabalhando.

## Informações Requisitadas

1. Escopo
 * Escopo bem definido e claro
 * Foi requisitado uma API com Endpoints para registrar usuários, projetos e o principal registrar tempo de trabalho pelos usuários em projetos.
2. Estimativa de horas
 * [Tabela detalhada com os tempos](http://git.vibbra.com.br/candidatos/CalebeRodriguesdeArruda/wikis/tempo-detalhado)
3. Tecnologias
 * Ruby
 * Rails framework web
 * Banco de Dados Postgresql
 * RSpec biblioteca para testes automatizados
 * Dentro do projeto Rails foram utilizados diversas gem (são bibliotecas que auxiliam o desenvolvimento), estão listadas no arquivo Gemfile
 * Autenticação via JWT conforme solicitado
4. Prazo
 * Horas de trabalho: 20 horas
 * Dias: 2 e meio de trabalho, considerando 8 horas continuas.
5. Projeto
 * Todas as atividades solicitadas foram concluídas.
 * Endpoints "MUST-HAVE" e "NICE-TO-HAVE" foram feitos e todos com testes automazidos para garantir o funcionamento.

## Aplicação em ambiente de Produção

A aplicação está disponível no Heroku para testes [Link base](https://ampulheta-api.herokuapp.com/api/v1)

Lembrando por ser uma API não existe pagina HTML no heroku "https://ampulheta-api.herokuapp.com/api/v1" 

* Usuário: ampulheta-api
* Password: Amp@26


## Documentação

A documentação da API está disponível na wiki deste repositório [Wiki - Documentação](http://git.vibbra.com.br/candidatos/CalebeRodriguesdeArruda/wikis/home)

## Getting Started

Primeiro deve possuir as tecnologinas instaladas: Ruby, Rails e Postgresql.

Realizar um clone do repositório.

```shell
git clone http://git.vibbra.com.br/candidatos/CalebeRodriguesdeArruda.git
```
```shell
cd CalebeRodriguesdeArruda
```
Realizar a instalação das dependências do projeto
```shell
>> CalebeRodriguesdeArruda/ bundle install
```
Para acessar o banco de dados, é necessário alterar o arquivo de configuração, conforme abaixo:
```ruby
CalebeRodriguesdeArruda/config/database.yml

development:
  <<: *default
  database: ampulheta-api_development
  username: #{incluir_usuario}
  password: #{incluir_password}
  host: localhost
  port: 5432
```
Após configurado deve ser realizado os comandos abaixo, criação do banco, script para criar dados, start server
```ruby
# Criar o banco de dados
>> CalebeRodriguesdeArruda/ rails db:create

# Criar o schema de tabelas no banco
>> CalebeRodriguesdeArruda/ rails db:migrate

# Executar script para dados iniciais no banco
>> CalebeRodriguesdeArruda/ rails db:seed

# Start server
>> CalebeRodriguesdeArruda/ rails s

# Em ambiente de desenvolvimento a API estará disponível localhost:3000/api/v1/....
```